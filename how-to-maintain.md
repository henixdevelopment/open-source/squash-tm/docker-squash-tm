This documentation is actually a FAQ. Without further ado : 


### Could you describe the layout ?

* build-context : the place where the Dockerfile and companion resources (scripts etc) reside. Those are the main assets, 
the other resources are mostly documentation.
* docker-compose-mariadb : an example of deployment with mariadb using docker-compose
* docker-compose-postgres : an example of deployment with postgresql using docker-compose
* docker-compose-reverseproxy : an example of deployment behind a reverse proxy using docker-compose
* tests : contains tests and their assets, in the form of a Jenkins pipeline. This pipeline doesn't produce images, it just runs tests.



### I am a human and I am creating an image. What tag should I use and where do I push ?

The shell commands are detailed in the companion file `how-to-release.md`, here we will focus on what you should do 
depending on what you intend to do.


A. I am just shipping a new version of Squash-TM to the QA

The qualifier for the TM version is thus `SNAPSHOT` or `RC`. The name of the image should be `docker.squashtest.org/squash-tm`
and the tag is the same as the binary distro you are shipping. 

B. I am shipping a public release

The qualifier of the TM version is thus `RELEASE`. You first need to perform the step `A.` described above. Then you will 
need to tag and push that image to DockerHub in the following cases : 

* in all cases, tag the image `squashtest/squash-tm:<major>.<minor>.<micro>`. Note that the qualifier `RELEASE` is not part of the tag.
* then, if the version of Squash-TM is the latest of its branch, tag the image `squashtest/squash-tm:<major>.<minor>-latest`.
* then, if this is the very latest version of Squash-TM, tag the image `squashtest/squash-tm:latest`.

Then push to DockerHub all those images as usual.  

C. I've modified the Dockerfile and/or scripts and I want to ship this to QA

In this case the version of Squash-TM doesn't matter, the docker image itself is the object of that release. 
You should tag the image as `docker.squashtest.org/docker-squash-tm:<short-git-hash>`,  
where `<short-git-hash>` are the first seven digits of the git hash. You can easily retrieve them with : `git rev-parse HEAD | cut -c1-7`.

See below for more details about developping the docker image.

### I am an automated build and I am creating an image, what should I do ? 

The information you are looking for are detailed in another project, see `https://bitbucket.org/squashtest/squashtest-tm-continuous-distribution`

### I am adding features to the image. Any guidelines ? 

The development branch is `dev` and the release branch is `master`. Development workflow is pretty much as usual : 
the features/bugs go to the branch `dev`, tests go to the `tests/Jenkinsfile` pipeline, ship to QA, then merge into 
branch `default` when automated and manual tests are green.

Some day you might need to create branches. 
Think again : if the startup script hasn't changed then you don't need a branch, and you should just keep 
targeting the `master` branch. On the other hand if the way of starting and/or configuring Squash-TM changed then you 
really do need branches (one for the old way, one for the new way). It may sound obvious, but in the past branches 
were created here just because new branches sprouted in Squash-TM; it was a bad idea and it should stop. 

The tests are scripted as a Jenkins pipeline, each stage being a test. Their goal is to test the behavior of the init 
scripts (currently it is `intall-script.sh`) in various context.
An important part of the test setup is the pod that will run the stage. There is one new pod for each stage. The pods 
are generated programatically using the modules `tests/vars/conf.groovy` and `tests/vars/templates.groovy`.
Most of the operations happening in stage steps are wrapped in groovy functions defined in 
`tests/vars/tools.groovy`, in order to improve reusability and readability, however Shell snippets inlined in the main 
pipeline script are still welcomed provided they are short enough.

And also last reminder, please document your changes :-O

   

