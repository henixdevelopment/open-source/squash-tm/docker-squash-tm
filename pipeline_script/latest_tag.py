import requests
import os
import re
import sys

from dotenv import load_dotenv
from looseversion import LooseVersion


load_dotenv()


DOCKER_USER = os.getenv('DOCKER_INTERNAL_REGISTRIES_USER')
DOCKER_PASSWORD = os.getenv('DOCKER_INTERNAL_REGISTRIES_PASSWORD')
DOCKER_IMAGE_NAME = os.getenv('DOCKER_IMAGE_NAME')
REGISTRY_URL= os.getenv('REGISTRY_URL')


def generate_latest_tag_env_entry(tag_list: list) -> str:
    """
    Sort a list of tag and return the most recent and format it (remove .RELEASE and add 'LATEST_TAG=' before, as it will be added to .env artifact)
    """

    tag_list.sort(key=LooseVersion)

    latest_tag = re.search(r'(\d+.\d+.\d+).RELEASE', tag_list[-1])
    
    return 'LATEST_TAG=' + latest_tag.group(1) if latest_tag.group(1) else None


def main():
    cred = requests.auth.HTTPBasicAuth(DOCKER_USER, DOCKER_PASSWORD)

    try:
        r = requests.get(auth=cred, url=f'https://{REGISTRY_URL}/v2/{DOCKER_IMAGE_NAME}/tags/list')

        # verify HTTP code
        if r.status_code != 200:
            sys.exit(f'HTTP Error: {r.status_code}: {r.text}')
        
        # verify json parsing
        try:
            tag_json = r.json()

        except ValueError as e:
            sys.exit(f'JSON parsing error: {e}')

    except requests.exceptions.RequestException as e:
        sys.exit(f'Error on API call: {e}')

    tag_list = [tag for tag in tag_json['tags']]

    # verify that list is not empty
    if not tag_list:
        sys.exit('Tag list is empty')
    
    return generate_latest_tag_env_entry(tag_list)

if __name__ == '__main__':
    main()
