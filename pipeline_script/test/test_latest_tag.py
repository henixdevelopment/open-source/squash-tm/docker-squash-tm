from pipeline_script import latest_tag as mut
# import pytest


TAG_LIST = [
    '6.1.0.RELEASE',
    '6.0.1.RELEASE',
    '5.1.1.RELEASE',
    '11.15.23.RELEASE',
    '5.0.2.RELEASE',
    '4.1.0.RELEASE',
    '4.0.2.RELEASE',
    '10.12.2.RELEASE',
    '3.0.5.RELEASE',
    '2.2.2.RELEASE',
    '2.1.6.RELEASE',
    '2.0.3.RELEASE',
    '10.0.2.RELEASE',
]

def test_generate_latest_tag_env_entry():
    assert mut.generate_latest_tag_env_entry(TAG_LIST) == 'LATEST_TAG=11.15.23'