#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2010 - 2012 Henix, henix.fr
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses/>.
#

# #############################################################################################
#
#  Global Environment variables :
#
#  | Variable                  | Description                             | Default     |
#  | --------------------------|:---------------------------------------:|:-----------:|
#  | JAVA_TOOL_OPTIONS         | Define JVM options                      | (unused)    |  
#  | SPRING_APPLICATION_JSON   | Set configuration of Spring boot process| (unused)    | 
#  | JAVA_XMS                  | Set the memory heap value at start      | 128m        | 
#  | JAVA_XMX                  | Set the max memory heap value           | 2048m       | 
#
#
#   Database Environment variables:
#
#
#  | Variable        | Description                           | Default H2  | Default MariaDB | Default Postgresql  |
#  | ----------------|:-------------------------------------:|:-----------:|:-------------:|:-------------------:|
#  | SQTM_DB_TYPE    | Either 'h2', 'mariadb' or 'postgresql'  | (special)   | (special)     | (special)           |
#  | SQTM_DB_HOST    | Hostname of the database server       | (unused)    | mariadb         | postgres            |
#  | SQTM_DB_PORT    | Port of the database sever            | (unused)    | 3306          | 5432                |
#  | SQTM_DB_NAME    | The name of the database              | (unused)    | squashtm      | squashtm            |
#  | SQTM_DB_SCHEMA *| The name of the schema                | (unused)    | $DB_NAME      | public              |
#  | SQTM_DB_USERNAME| The username for Squash-TM            | (unused)    | root          | root                |
#  | SQTM_DB_PASSWORD| The password for Squash-TM            | (unused)    | (none)        | (none)              |
#  | SQTM_DB_SSLMODE | SSL Mode for Postgres database        | (unused)    | (unused)      | null                |
#
#   notes about default values:
#   - (none) : the variable is mandatory and has no default value.
#   - (special) : see below.
#   - (*) : experimental, should be stable but has not yet gone through thorough testing.
#   - (unused) :  the variable has no meaning for this database server.
#                 In the case of H2 those values are hardcoded to Squash-TM default internal parameters.
#
#   Variable SQTM_DB_TYPE and deprecated variables:
#   -----------------------------------------------
#   'SQTM_DB_TYPE' has an impact on the default values for several others. The best practice is to define it explicitly.
#   However in previous version of the image this variable was internal only and its value was implied by the existence
#   of other variables which are now legacy (see below).
#   This mechanism is still maintained for now, eg if 'SQTM_DB_TYPE' is undefined but any variable of the form
#   'MYSQL_ENV_*' or 'POSTGRES_ENV_*' then the value of 'SQTM_DB_TYPE' will be implicitly resolved. However, variables
#   of the form 'SQTM_*' will take precedence. Also be aware that this secondary mechanism will effectively stop working
#   once the deprecated variables are removed eventually.
#
#
#   Deprecated Environment Variables :
#   ------------------------------
#
#   The variable 'MYSQL_ENV_MYSQL_ROOT_PASSWORD' has been removed and is not supported anymore.
#
#   The following variable are still supported, although we strongly recommend to move to the new variables.
#
#   Deprecated MySQL/MariaDB-specific Environment variables :
#
#   - MYSQL_ENV_MYSQL_USER : the username of Squash-TM service account on MySQL
#   - MYSQL_ENV_MYSQL_PASSWORD : the password that goes along that user account
#   - MYSQL_ENV_MYSQL_DATABASE : the name of the schema on MySQL
#
#   Deprecated Postgresql-specific Environment variables :
#
#   - POSTGRES_ENV_POSTGRES_USER : the username of Squash-TM service account on Postgres
#   - POSTGRES_ENV_POSTGRES_PASSWORD : the password that goes along that user account
#   - POSTGRES_ENV_POSTGRES_DB : the name of the database on the postgresql cluster (server)
#
#
#
# #############################################################################################

cd /opt/squash-tm/bin

# Default variables
JAR_NAME="squash-tm.war"  # Java main library
HTTP_PORT=8080                             # Port for HTTP connector (default 8080; disable with -1)
# Directory variables
TMP_DIR=../tmp                             # Tmp and work directory
BUNDLES_DIR=../bundles                     # Bundles directory
CONF_DIR=../conf                           # Configurations directory
LOG_DIR=../logs                            # Log directory
TOMCAT_HOME=../tomcat-home                  # Tomcat home directory
PLUGINS_DIR=../plugins                     # Plugins directory

## Do not configure a third digit here
REQUIRED_VERSION=17

# Squash TM DB Upgrade
DB_UPDATE_SCRIPTS_DIRECTORY=/opt/squash-tm/database-scripts
MAX_TM_VERSION_UPGRADED_VIA_SHELL="8.1.0"


########## SEARCHING FOR LINKED DB SERVER ############
##########      PREPARING CONNECTION      ############

# Locate if the legacy variables are set
LEGACY_MYSQL_TYPE=$(env | grep MYSQL_ENV | head -n 1 | sed s/.*/mariadb/g)
LEGACY_PG_TYPE=$(env | grep POSTGRES_ENV | head -n 1 | sed s/.*/postgresql/g)

# Chain resolution of db_type by order of priority :
# 1/ variable explicitly set,
# 2/ either 'mariadb' or 'postgresql' if legacy variables are set
# 3/ h2 as default
db_type=${SQTM_DB_TYPE:-${LEGACY_MYSQL_TYPE:-${LEGACY_PG_TYPE:-'h2'}}}

case "$db_type" in
    "postgresql")
      db_host=${SQTM_DB_HOST:-'postgres'}
      db_port=${SQTM_DB_PORT:-'5432'}
      db_name=${SQTM_DB_NAME:-${POSTGRES_ENV_POSTGRES_DB:-'squashtm'}}
      db_username=${SQTM_DB_USERNAME:-${POSTGRES_ENV_POSTGRES_USER:-'root'}}
      db_password=${SQTM_DB_PASSWORD:-${POSTGRES_ENV_POSTGRES_PASSWORD}}
      db_schema="${SQTM_DB_SCHEMA:-public}"
      db_sslmode=${SQTM_DB_SSLMODE:-''}
      db_url="jdbc:${db_type}://${db_host}:${db_port}/$db_name?currentSchema=$db_schema"
      [[ ! -z $db_sslmode ]] && db_url="${db_url}&sslmode=${db_sslmode}" && DB_CONN_SSL="?sslmode=${db_sslmode}"
      ;;
    "mariadb")
      db_host=${SQTM_DB_HOST:-'mariadb'}
      db_port=${SQTM_DB_PORT:-'3306'}
      db_name=${SQTM_DB_NAME:-${MYSQL_ENV_MYSQL_DATABASE:-'squashtm'}}
      db_username=${SQTM_DB_USERNAME:-${MYSQL_ENV_MYSQL_USER:-'root'}}
      db_password=${SQTM_DB_PASSWORD:-${MYSQL_ENV_MYSQL_PASSWORD}}
      db_schema="${SQTM_DB_SCHEMA:-${db_name}}"

      db_url="jdbc:${db_type}://${db_host}:${db_port}/$db_name"
      ;;
    "h2")
      echo 'his squash instance will use the default embedded h2 DB. This is OK for test purpose only'.
      echo 'TO USE SQUASH TM IN PRODUCTION, IT IS RECOMMENDED TO USE EITHER MARIADB OR POSTGRESQL DATABASE'
      db_host='..'
      db_username='sa'
      db_password='sa'
      db_name='squashtm'
      db_url='jdbc:h2:../data/squash-tm;NON_KEYWORDS=ROW,VALUE' # after 5.0.0 add ;NON_KEYWORDS=ROW,VALUE
      ;;
    *)
      echo "ERROR:   SQTM_DB_TYPE: ${db_type} is not supported or wrong input."
      echo "ERROR:   Please use one of the following values for 'SQTM_DB_TYPE' variable: mariadb, postgresql, h2"
      echo "ERROR:   Stopping boot phase"
      echo "ERROR:   Squash-TM didn't start"
      exit 1
      ;;
esac

# validate that a password was supplied
echo ${db_password:?'Error : no database password is set'} > /dev/null

cat << EOF
Resolved configuration :

DB_TYPE       $db_type
DB_HOST       $db_host
DB_PORT       $db_port
DB_NAME       $db_name
DB_SCHEMA     $db_schema
DB_USERNAME   $db_username
DB_PASSWORD   ************
DB_SSLMODE    $db_sslmode
DB_URL        $db_url
EOF

# Also, export the resolved configuration in /opt/squash-tm/bin/docker-resolved-env for convenience
cat << EOF > /opt/squash-tm/bin/docker-resolved-env
export DB_TYPE="$db_type"
export DB_HOST="$db_host"
export DB_PORT="$db_port"
export DB_NAME="$db_name"
export DB_SCHEMA="$db_schema"
export DB_USERNAME="$db_username"
export DB_PASSWORD="$db_password"
export DB_SSLMODE="$db_sslmode"
export DB_URL="$db_url"
EOF

echo
echo


########## CHECKING SQUASH TM DB EXISTENCE ###########
##########       EXECUTING DB SCRIPTS      ###########

####### Definitions

# Parameters : none
wait_for_db(){
  case "$db_type" in
    'mariadb') until mysqladmin status --protocol tcp -u "$db_username" -p"$db_password" -h "$db_host" -P "$db_port"; do echo waiting for mariadb; sleep 2; done; ;;
    'postgresql') until pg_isready -d ${db_name} -h ${db_host} -p ${db_port} -U ${db_username} -t 5; do echo waiting for postgresql; sleep 2; done; ;;
  esac
}

# Parameters : $1 the path to the script to run
run_script(){
  case "$db_type" in
    'mariadb') mariadb --protocol tcp  -h "$db_host" -u "$db_username" -p"$db_password" -P "$db_port" "$db_name" < "$1" ;;
    'postgresql')
      PGOPTIONS="--search_path=${db_schema}"  psql -v ON_ERROR_STOP=1 postgresql://${db_username}:${db_password}@${db_host}:${db_port}/${db_name}${DB_CONN_SSL} -f "$1" ;;
  esac
}

# Parameters : $1 the query
run_query(){
  case "$db_type" in
    'mariadb') mariadb --protocol tcp  -h "$db_host" -u "$db_username" -p"$db_password" -P "$db_port" "$db_name" -e "$1" ;;
    'postgresql')
      PGOPTIONS="--search_path=${db_schema}"  psql -v ON_ERROR_STOP=1 postgresql://${db_username}:${db_password}@${db_host}:${db_port}/${db_name}${DB_CONN_SSL} -c "$1" ;;
  esac
}

find_db_upgrade_scripts(){

  db_schem_version=$(run_query "SELECT * FROM CORE_CONFIG WHERE STR_KEY='squashtest.tm.database.version';" |  grep -Eo '(\d+\.?){2,}')

  # find all the scripts
  find "${DB_UPDATE_SCRIPTS_DIRECTORY}" | \
    # retain only those that corresponds to this db_type and are upgrades scripts
    grep "${db_type}-upgrade" |  \
    # prepend the output with version numbers, which will be our sorting keys  : "<major> <minor> <micro> <scriptname>"
    sed -E 's/(.*-([0-9]+)\.([0-9]+)\.([0-9]+)\.sql)/\2 \3 \4 \1/g' | \
    # left-pad with zeroes the version numbers so that a lexicographic sort will work just like semantic versioning
    awk '{printf "%05d %05d %05d %s\n", $1, $2, $3, $4;}' | \
    # now we can sort
    sort | \
    # remove the sorting key
    cut -d ' ' -f 4 | \
    # retain all the scripts that match the current schema version and all those after it
    grep -F "$db_schem_version" -A 10000 | \
    # retain all the scripts before the 8.1.0
    grep -F "$MAX_TM_VERSION_UPGRADED_VIA_SHELL" -B 10000 | \
    # remove the first script of the current schema version
    awk 'NR>1'
}


###### Execution


echo "testing : is database present"
echo "Targeting ${db_host}:${db_port}($db_type)"
echo "Waiting until database is available..."

wait_for_db

echo 'Database is up. Is it new ?'

# MariaDB likes its tablenames uppercase while PG likes it lowercase
query_db_exist="SELECT 1 FROM information_schema.tables WHERE table_schema = '${db_schema}' AND (table_name = 'ISSUE' OR table_name = 'issue');"
if ! run_query "$query_db_exist" | grep 1 > /dev/null; then
  echo 'The database is empty, it will be initialized by Squash'

else
  echo "An existing schema was found. Is an upgrade necessary ?"

  pending_upgrades=$(find_db_upgrade_scripts)

  if [ "${#pending_upgrades}" = 0 ]; then
    echo "Database already up to date"
  else
    echo "unrolling scripts : "
    for sc in $pending_upgrades; do
      echo applying "$sc";
      run_script "$sc"
    done
    echo "Database upgrade complete"
  fi
fi

### JAVA OPTIONS ###

app_xms="-Xms${JAVA_XMS:-128m}"
app_xmx="-Xmx${JAVA_XMX:-2048m}"
echo
echo "JAVA XMS: ${app_xms}"
echo "JAVA_XMX: ${app_xmx}"

# Extra Java vm args
java_vm_args="${app_xms} ${app_xmx} -server"

########## PREPARE PLUGINS BEFORE START ###########
###################################################

# The start-plugins.cfg is a file in the conf folder.
# If it by default in the image and contains the list of the possible plugins that are embedded in the image.
# All the plugins are listed and to activate a plugin, the user uncomments the line. (Delete the # in front of the line)
# Ex: |# apt-rest ---> |api-rest (where | is the beginning of the file)

if [ -f "/opt/squash-tm/conf/start-plugins.cfg" ]
then
  echo
  echo
  echo "**Starting plugins on boot**"
  echo
  echo "--> Found start-plugins.cfg file. Parsing through it to find the plugins:"
  touch /opt/squash-tm/plugins/activated_plugins.txt # File for storing the plugins that are successfully moved
  while read plugin  || [ -n "$plugin" ] # Read each line of the start-plugins.cfg as plugin
  do
    case $plugin in
      ''|\#*) continue;; # Skip the lines with # and empty lines
    esac
    echo "$plugin"
    value=$(find /opt/squash-tm/plugin-files/"$plugin"* | sed -n 1p)
    if [ ! "$value" ]; then # Check of mispelled plugins. If mispelling --> break.
      echo "ERROR: $plugin does not exist or wrong name. Stop the process"
      exit 
    else
      # The plugin id is not the necessarly the complete name of the plugin. 
      # Therefore we use the find to auto complete the name (bourne shell limitation with the *).
      plugin_name=$(find /opt/squash-tm/plugin-files/"$plugin"* | sed -n 1p);
      # Use of the cp and not the mv for access right limitations and possible docker stop/start possibilities  
      cp -r $(find /opt/squash-tm/plugin-files/"$plugin"* | sed -n 1p) "/opt/squash-tm/plugins/";
      # Clean up the plugin_name to :
      # 1) Check its existance in the final plugins directory
      # 2) Better human reading of the log
      plugin_name=$(printf '%s' "$plugin_name" | sed -e 's/\/opt\/squash-tm\/plugin-files\///')
      # Check if hte copy is successfull
      if [ -d "/opt/squash-tm/plugins/$plugin_name/" ]
      then 
        echo "$plugin_name" >> /opt/squash-tm/plugins/activated_plugins.txt
      fi
    fi
   done < /opt/squash-tm/conf/start-plugins.cfg;
  echo
  # If activated_plugins.txt is not empty --> print it in the log
  if [ -s /opt/squash-tm/plugins/activated_plugins.txt ]
  then 
    echo "--> The list of copied plugins into plugins directory"
    cat /opt/squash-tm/plugins/activated_plugins.txt
  else
    echo "WARNING: No plugins were copied into plugins directory, procede with only core plugins"
  fi
else
  echo
  echo
  echo "Starting plugins on boot"
  echo "WARNING: start-plugins.cfg not found on path /opt/squash-tm/conf/start-plugins.cfg."
  echo "Squash boot up with its core plugins"
fi
echo
echo "End of starting plugin phase"
echo
echo

##########           IN SQUASH-TM            ###########

#That script will :
#- check that the java environnement exists,
#- the version is adequate,
#- will run the application

# Tests if java exists
echo -n "$0 : checking java environment... ";

java_exists=`java -version 2>&1`;

if [ $? -eq 127 ]
then
    echo;
    echo "$0 : Error : java not found. Please ensure that java is installed in \$PATH";
    exit -1;
fi

echo "done";

# Create logs and tmp directories if necessary
if [ ! -e "$LOG_DIR" ]; then
    mkdir $LOG_DIR
fi

if [ ! -e "$TMP_DIR" ]; then
    mkdir $TMP_DIR
fi

# Tests if the version is high enough
echo -n "checking version... ";

numeric_required_version=`echo $REQUIRED_VERSION |sed 's/\./0/g'`;
java_version=`echo $java_exists | grep version |cut -d " " -f 3  |sed 's/\"//g' | cut -d "." -f 1,2 | sed 's/\./0/g'`;

if [ $java_version -lt $numeric_required_version ]
then
    echo;
    echo "$0 : Error : your JRE does not meet the requirements. Please install a new JRE, required version ${REQUIRED_VERSION}.";
    exit -2;
fi

echo  "done";

# Let's go !
echo "$0 : starting Squash TM... ";

#That script will :
#- check that the java environnement exists,
#- the version is adequate,
#- will run the application

# Tests if java exists
echo -n "$0 : checking java environment... ";

java_exists=`java -version 2>&1`;

if [ $? -eq 127 ]
then
    echo;
    echo "$0 : Error : java not found. Please ensure that java is installed in \$PATH";
    exit -1;
fi

echo "done";

# Create logs and tmp directories if necessary
if [ ! -e "$LOG_DIR" ]; then
    mkdir $LOG_DIR
fi

if [ ! -e "$TMP_DIR" ]; then
    mkdir $TMP_DIR
fi

# Tests if the version is high enough
echo -n "checking version... ";

numeric_required_version=`echo $REQUIRED_VERSION |sed 's/\./0/g'`;
java_version=`echo $java_exists | grep version |cut -d " " -f 3  |sed 's/\"//g' | cut -d "." -f 1,2 | sed 's/\./0/g'`;

if [ $java_version -lt $numeric_required_version ]
then
    echo;
    echo "$0 : Error : your JRE does not meet the requirements. Please install a new JRE, required version ${REQUIRED_VERSION}.";
    exit -2;
fi

echo  "done";

# Let's go !
echo "$0 : starting Squash TM... ";

export APP_OPTS="-Dspring.datasource.url=${db_url} -Dspring.datasource.username=${db_username} -Dspring.datasource.password=${db_password} -Duser.language=en"
daemon_args="${java_vm_args} ${APP_OPTS} ${JAVA_TOOL_OPTIONS} -Djava.io.tmpdir=${TMP_DIR} -Dlogging.dir=${LOG_DIR} -jar ${BUNDLES_DIR}/${JAR_NAME} --spring.profiles.active=${db_type} --spring.config.additional-location=${CONF_DIR}/ --spring.config.name=application,squash.tm.cfg --logging.config=${CONF_DIR}/log4j2.xml --squash.path.bundles-path=${BUNDLES_DIR} --squash.path.plugins-path=${PLUGINS_DIR} --server.port=${HTTP_PORT} --server.tomcat.basedir=${TOMCAT_HOME} "

exec java ${daemon_args}