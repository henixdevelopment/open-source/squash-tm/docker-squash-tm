*** Settings ***
Test Setup    Setup
Test Teardown    Teardown
Suite Setup    Suite Setup
Suite Teardown    Suite Teardown
Resource    tools.resource
Test Timeout    5 minutes

*** Variables ***
${DB_IMAGE}    mariadb:10.6
${DB_TYPE_EXPECTED}    mariadb
${DOCKER_NETWORK}    squash_tm_mariadb
${CONTAINER_ID_SQUASH}    ${EMPTY}

*** Test Cases ***
MariaDB Deloyed As Documentation Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                    ...    -e SQTM_DB_TYPE=mariadb
                    ...    -e SQTM_DB_USERNAME=squashtm
                    ...    -e SQTM_DB_PASSWORD=MustB3Ch4ng3d
                    ...    -e SQTM_DB_NAME=squashtm
                    ...    -p 8080:8080
    Deploy Squash Should Pass    ${env_squash}    external_db=${TRUE}

MariaDB With Old Variables Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                    ...    -e MYSQL_ENV_MYSQL_USER=squashtm
                    ...    -e MYSQL_ENV_MYSQL_PASSWORD=MustB3Ch4ng3d
                    ...    -e MYSQL_ENV_MYSQL_DATABASE=squashtm
                    ...    -p 8080:8080
    Deploy Squash Should Pass    ${env_squash}    external_db=${TRUE}
