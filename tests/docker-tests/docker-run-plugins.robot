*** Settings ***
Test Setup    Setup
Test Teardown    Teardown
Suite Setup    Suite Setup
Suite Teardown    Suite Teardown
Resource    tools.resource
Test Timeout    5 minutes

*** Variables ***
${LICENCE_DIR}    /opt/squash-tm/plugins/license/squash-tm.lic
${CONFIG_DIR}    /opt/squash-tm/conf/start-plugins.cfg
${DOCKER_NETWORK}    h2
${CONTAINER_ID_SQUASH}    ${EMPTY}
${PROJECT_REPO}    test-tm-docker-image
${DB_TYPE_EXPECTED}    h2
${DB_IMAGE}    ${EMPTY}
${TESTS_DIRECTORY}    docker-squash-tm/tests/docker-tests

*** Test Cases ***
Run With Community Plugins Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                            ...    -p 8080:8080
                            ...    --mount type=bind,source="$(pwd)"/${TESTS_DIRECTORY}/test-community/start-plugins.cfg,target=${CONFIG_DIR}
    Deploy Squash With Plugins Should Pass    ${env_squash}    plugin_to_find=bugzilla

Run With Premium Plugins Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                            ...    -p 8080:8080
                            ...    --mount type=bind,source="$(pwd)"/${TESTS_DIRECTORY}/test-premium/start-plugins.cfg,target=${CONFIG_DIR}
                            ...    --mount type=bind,source="$(pwd)"/${TESTS_DIRECTORY}/test-premium/squash-tm.lic,target=${LICENCE_DIR}
    Deploy Squash With Plugins Should Pass    ${env_squash}    plugin_to_find=squash-tm-premium

Run With Ultimate Plugins Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                            ...    -p 8080:8080
                            ...    --mount type=bind,source="$(pwd)"/${TESTS_DIRECTORY}/test-ultimate/start-plugins.cfg,target=${CONFIG_DIR}
                            ...    --mount type=bind,source="$(pwd)"/${TESTS_DIRECTORY}/test-ultimate/squash-tm.lic,target=${LICENCE_DIR}
    Deploy Squash With Plugins Should Pass    ${env_squash}    plugin_to_find=workflow-automjira
