*** Settings ***
Test Setup    Setup
Test Teardown    Teardown
Suite Setup    Suite Setup
Suite Teardown    Suite Teardown
Resource    tools.resource
Test Timeout    5 minutes

*** Variables ***
${DB_IMAGE}    postgres:13
${DB_TYPE_EXPECTED}    postgresql
${DOCKER_NETWORK}    squash_tm_postgresql
${CONTAINER_ID_SQUASH}    ${EMPTY}

*** Test Cases ***
PostgreSQL Deloyed As Documentation Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                    ...    -e SQTM_DB_TYPE=${DB_TYPE_EXPECTED} 
                    ...    -e SQTM_DB_USERNAME=squashtm
                    ...    -e SQTM_DB_PASSWORD=MustB3Ch4ng3d
                    ...    -e SQTM_DB_NAME=squashtm
                    ...    -p 8080:8080
    Deploy Squash Should Pass    ${env_squash}    external_db=${TRUE}

PostgreSQL With Old Variables Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                    ...    -e POSTGRES_ENV_POSTGRES_USER=squashtm
                    ...    -e POSTGRES_ENV_POSTGRES_PASSWORD=MustB3Ch4ng3d
                    ...    -e POSTGRES_ENV_POSTGRES_DB=squashtm
                    ...    -p 8080:8080
    Deploy Squash Should Pass    ${env_squash}    external_db=${TRUE}

Postgresql With Sslmode Should Pass
    [Documentation]    This test required a PostgreSQL database configured with ssl connexion. We only test the sslmode variable.
    ${ip_db}=    Deploy DB Should Pass
    ${env_squash}=    Catenate    --network ${DOCKER_NETWORK}
                    ...    -e SQTM_DB_HOST=${ip_db}
                    ...    -e SQTM_DB_TYPE=${DB_TYPE_EXPECTED} 
                    ...    -e SQTM_DB_USERNAME=squashtm
                    ...    -e SQTM_DB_PASSWORD=MustB3Ch4ng3d
                    ...    -e SQTM_DB_NAME=squashtm
                    ...    -e SQTM_DB_SSLMODE=require
                    ...    -p 8080:8080
    ${squash_id}=    Run Shell Docker CLI    run -d -t ${env_squash} ${DOCKER_IMAGE}     fail_on_error=${TRUE}
    ${jdbc_sslmode}=    Run Shell Docker CLI    logs ${squash_id}    grep_args="public&sslmode=require"
    Should Not Be Empty    ${jdbc_sslmode}
