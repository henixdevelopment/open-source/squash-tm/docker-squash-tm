*** Settings ***
Test Setup    Setup
Test Teardown    Teardown
Suite Setup    Suite Setup
Suite Teardown    Suite Teardown
Resource    tools.resource
Test Timeout    5 minutes

*** Variables ***
${DB_TYPE_EXPECTED}    h2
${DOCKER_NETWORK}    h2
${CONTAINER_ID_SQUASH}    ${EMPTY}
${DB_IMAGE}    ${EMPTY}

*** Test Cases ***
  
H2 Without Variables Should Pass
    ${env_squash} =    Catenate   --network ${DOCKER_NETWORK}  
    Deploy Squash Should Pass    ${env_squash}

H2 Run With type=h2 Should Pass
    ${env_squash}=    Catenate   --network ${DOCKER_NETWORK}
                    ...    -e SQTM_DB_TYPE=${DB_TYPE_EXPECTED} 
    Deploy Squash Should Pass    ${env_squash}

H2 SQTM_DB Variables Should Pass
    ${env_squash} =    Catenate   --network ${DOCKER_NETWORK}    
                    ...    -e SQTM_DB_TYPE="h2"
                    ...    -e SQTM_DB_HOST="should_not_be_read"
                    ...    -e SQTM_DB_PORT="should_not_be_read"
                    ...    -e SQTM_DB_NAME="should_not_be_read"
                    ...    -e SQTM_DB_USERNAME="should_not_be_read"
                    ...    -e SQTM_DB_PASSWORD="should_not_be_read"
                    ...    -e SQTM_DB_SCHEMA="should_not_be_read"
                    ...    -e SQTM_DB_SSLMODE="should_not_be_read"
    Deploy Squash Should Pass    ${env_squash}

