# Test-tm-docker-image

## How is it structured

The tests are defined in this repository as `.robot` files.  
They all inclued the `tools.ressource` file that encapsulate docker bash command, setup and teardown.  
Tests cases are written in the Squash acceptance instance : <https://recette.squashtest.org/squash/test-case-workspace/test-case-library/90/content?anchor=dashboard>.  
Directories as `test-community|premium|ultimate` are resources for the docker-run-plugins test cases.

## How it work

The tests are exectued on a temporary image of squash starting with a tag : `test-<buildtype>-<date>`.
Is receives the compulsory DOCKER_TAG variable and tests the docker image as followed:
![Workflow of the automated tests](image.png)

The DevOps workflow is defined in the `workflow.yaml`

## Pipeline variables

* LICENCE_PREMIUM
* LICENCE_ULTIMATE
* OPENTFCONFIG
* TM_PASSWORD
* TM_USER
* TOKEN (prod orchestrator token)